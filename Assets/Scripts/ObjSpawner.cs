using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class ObjSpawner : MonoBehaviour
{
	//public GameObject spawnPrefab;
	public AssetReference spawnPrefab;

	private void Start()
	{
		

	}

	private void Update()
	{
		if(Input.GetButtonDown("Jump"))
		{

			StartCoroutine(SpawnTarget());
			//AsyncOperationHandle<GameObject> temp =
			//spawnPrefab.LoadAssetAsync<GameObject>();

			//temp.Completed += (a) =>
			//{
			//	Instantiate(a.Result, transform.position, Quaternion.identity);
			//};
			//UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle<GameObject> temp = Addressables.InstantiateAsync("GermSlimePrefab", transform, false);

			//Addressables.Release(temp);
			
			//spawnPrefab.InstantiateAsync(transform, false);

			//Instantiate(spawnPrefab, transform.position, Quaternion.identity);
		}
	}

	IEnumerator SpawnTarget()
	{
		if(spawnPrefab.Asset == null)
		{
			AsyncOperationHandle<GameObject> temp =
				spawnPrefab.LoadAssetAsync<GameObject>();
			yield return temp;
		}

		if(spawnPrefab.Asset != null)
		{
			Instantiate(spawnPrefab.Asset, transform.position, Quaternion.identity);
		}
	}
}
